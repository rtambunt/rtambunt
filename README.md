# Hi, I'm Robbie !

I'm currently...

🏗️&nbsp; Building a mobile social app called **Where Now** 

👏&nbsp; Proud of my mixology recipe app called **On the Rocks**

✏️&nbsp; Deepening my knowledge of **React Native**, **Node.js**, **Mongodb**




<br>

➡️ &nbsp; Learn more about me here! : https://robbietambunting.com


## Skills
**Frontend:** React, Next.js, TypeScript, JavaScript (ES6+), CSS

**Backend:** Node.js, Express.js, Python, FastAPI, Supabase

**Database:** MongoDB, PostgreSQL, T-SQL

**Tools:** Tailwind, Tanstack Query, React Router, Redux  

**Deployment:** Vercel, Render, Google Cloud

**Other:** C++

## Education
B.A Computational Mathematics - University of California, Santa Cruz
